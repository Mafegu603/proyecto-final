import sqlite3
class DBAdmin:
    
    def __init__(self):
        self.conex=sqlite3.connect('procesos.db')
        self.c=self.conex.cursor()
         
    def ConsultarTabla(self):
        self.c.execute('''SELECT * FROM procesos''')
        print (self.c)
        datos=self.c.fetchone()
        print(datos)

    def crearRegistro(self):
        self.c.execute('''INSERT OR IGNORE INTO procesos VALUES (?,?,?)''')
        self.conex.commit()
        

    def editarRegistro(self):
        self.c.execute('''UPDATE procesos SET NOMBRE = ?, ID_PROCESO = ?, NOTARIA = ?''')
        self.conex.commit() 

    def borrarRegistro(self):
        self.c.execute('''DELETE FROM PROCESOS WHERE NOMBRE = ? ''')
        self.conex.commit()
        
 

    def crearTabla(self):
        self.c.execute('''CREATE TABLE IF NOT EXISTS Procesos(
            NOMBRE text,
            ID_PROCESO real,
            NOTARIA real,
            PRIMARY KEY(ID_PROCESO)

        )''')

       

        self.c.execute('''CREATE TABLE IF NOT EXISTS Documentos(
            NOMBRE DOC text,
            ID_PROCESO real,
            FOREIGN KEY (ID_PROCESO) REFERENCES Procesos(ID_PROCESO)
        )''')

 

        self.c.execute('''CREATE TABLE IF NOT EXISTS Presupuesto(
            ID_PROCESO real,
            VALOR_PRESUPUESTO real,
            FOREIGN KEY (ID_PROCESO) REFERENCES Procesos(ID_PROCESO)
        )''')

 

        self.c.execute('''CREATE TABLE IF NOT EXISTS Gastos(
            ID_PROCESO real,
            NOMBRE_GASTO real,
            VALOR_GASTO real,
            FOREIGN KEY (ID_PROCESO) REFERENCES Procesos(ID_PROCESO)

        )''')

 
